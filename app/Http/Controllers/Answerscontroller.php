<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAnswerRequest;
use App\Http\Requests\StoreQuestionRequest;
use App\Http\Requests\UpdateAnswerRequest;
use App\Models\Answer;
use App\Models\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Answerscontroller extends Controller
{
    public function index()
    {


    }
//add new answer
    public function store(StoreAnswerRequest $request) //request= backend by request input al user. StoreAnswerRequest= custom rules
    {
        $answer = new Answer(); //answer gededa (create new object = create new answer)
        $answer->body = $request->body; //ay requestt bykon gayly geded msh already 3ndy fi al database = input al user
        $answer->quesId = $request->id; //al forign key bta3 al answer (quesId) = al id aly galy mn al user
        $answer->save();
        return $answer;

        //return 'store fi answer controller';
    }

    public function update(UpdateAnswerRequest $request, Answer $answer) //Update answer
    {
        //return 1;
        $answer->body= $request->body; //update value al answer aly 3ndy mn al database b al answer al gededa(user input)
        $answer->save();
        return "Record updated successfully.";
    }

    public function destroy(Answer $answer)
    {
        $answer->delete();
        return 'Answer destroyed successfully';
        //return 'destroy fi question controller';
    }
}


