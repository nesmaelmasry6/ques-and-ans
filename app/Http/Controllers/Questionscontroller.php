<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreQuestionRequest;
use App\Http\Resources\QuestionResource;
use App\Models\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Questionscontroller extends Controller
{

    public function index(Request $request) //show all questions and search by question body
    {
        $questions = Question::where('body','LIKE','%'.$request->name.'%') //search b al name aly dakhly (request->name) between al question body
        ->orderBy('created_at')->withCount('answers')
               ->get();

               return QuestionResource::collection($questions);
    }

    public function show(Question $question) //show one question with it's answers
    {
        return Question::with('answers')->where('id',$question->id)->first();
        return new QuestionResource(Question::with('answers')->find($question->id)->first()->load('answers'));
       // return ['question'=>$question,'answers'=>$question->answers];
    }

    public function store(StoreQuestionRequest $request) //add question
    {
        $temp = Question::where('body',$request->body)->first();
        if($temp) return 'question already exist'; //search fi database lw al question already exsit. awel identical case hayrg3 false

        //spam limits. limited questions per day
        $count = Question::whereDate('created_at', '=', date('Y-m-d'))->count(); //y3ed as2la fi youm wahed
        if ($count >=50) return 'you reached the question limit for today'; //aktr mn 50 won't get posted

        $question = new Question(); //add question 3ady
        $question->body = $request->body;
        $question->save();

        return $question;
    }

    public function destroy(Question $question)
    {
        $question->delete();
        return 'Question destroyed successfully';
        //return 'destroy fi question controller';
    }

}
