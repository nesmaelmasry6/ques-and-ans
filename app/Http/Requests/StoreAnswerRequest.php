<?php

namespace App\Http\Requests;

use App\Models\Question;
use Illuminate\Foundation\Http\FormRequest;

class StoreAnswerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $questions=Question::all()->implode('id',','); // btktb al format aly (in) byfhmo. ex: 1,2,3 = 'id' ',' 'id' ','
        return [
            "body" =>"required|min:1"
            ,"id"=> "required|in:".$questions

        ];
    }
}
