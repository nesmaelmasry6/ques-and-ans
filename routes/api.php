<?php

use App\Http\Controllers\Questionscontroller;

use App\Http\Controllers\Answerscontroller;
use App\Http\Resources\QuestionResource;
use App\Models\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/questions',[Questionscontroller::class,'index']);
Route::get('/questions/{question}',[Questionscontroller::class,'show']);
Route::post('/questions',[Questionscontroller::class,'store']);
Route::delete('/questions/{question}',[Questionscontroller::class,'destroy']);


Route::post('/answers',[Answerscontroller::class,'store']);
Route::put('/answers/{answer}',[Answerscontroller::class,'update']);
Route::delete('/answers/{answer}',[Answerscontroller::class,'destroy']);

//get question with api resource
Route::get('/question/{id}', function ($id) {
    return new QuestionResource(Question::findOrFail($id));
});

